module.exports = {
  /**
   * Application configuration section
   * http://pm2.keymetrics.io/docs/usage/application-declaration/
   */
  apps : [

    {
      name      : 'wsDNRPA',
      script    : 'app.js',
      
      watch: false,
      max_memory_restart: '1G',
      error_file: './LOG/err.log',
      out_file: './LOG/out.log',
      env: {
        NODE_ENV: 'development'
      },
      env_production: {
        NODE_ENV: 'production'
      }
    }]
  };