const soap = require('soap');
const fs = require('fs');
var { redisClient } = require('../database/config');
//const Patente = require('../models/patente');
const test = require('../tests/test-script');


module.exports.getPatente = async function(req, res) {

    const { patente } = req.params;
    const ip = req.headers['x-forwarded-for'] || req.connection.remoteAddress.slice(7);

    /* SI LA CONEXION A LA DB FALLA, REALIZA LA CONSULTA A DNRPA SIN CACHEAR */
    if(redisClient.ready==false){
        console.log('ERROR: No se pudo conectar a la DB. Consultando a DNRPA:');
        soap.createClient('https://www.ws1.dnrpa.gov.ar/WS-LEGAUTO/WS-LEGAUTO.wsdl', {
                wsdl_options: {
                    cert: fs.readFileSync('certs/certificate2.pem'),
                    key: fs.readFileSync('certs/key.key'),
                    passphrase: 'dp19ba84'
                }

            }, function(err, client) {
                if (err) {
                    console.log('ERROR: Se produjo un error en el servicio SOAP: ', err);
                }
                client.setSecurity(new soap.ClientSSLSecurityPFX(
                    fs.readFileSync('certs/30681617783.p12'),
                    'dp19ba84', {
                        strictSSL: false,
                        rejectUnauthorized: false,
                        forever: true,
                    },
                ));
                client.consultarDominio({
                    dominio: patente,
                    usuario: 'msm'
                }, function(err, result, raw, soapHeader) {
                    if (err) {
                        console.error(`ERROR: Fallo al consultar la patente ${patente} - ${new Date().toLocaleString()}'`);
                        return res.send(err)
                    }
                    if (result) {
                        res.json(result);
                        return;
                    }
                });

            });
    }
    /* LA CONEXION A LA DB FUE EXITOSA */
    else{
        try {
            // Verifico si la patente esta cacheada
            const patenteCacheada = await redisClient.getAsync(patente);
            //console.log(`INFO: La patente ${patente} esta cacheada:`, patenteCacheada);
    
    
            if (patenteCacheada == null) {
                console.log(`INFO: ${new Date().toLocaleString()}  La patente ${patente} NO esta en cache, consultando a DNRPA...`);
    
                soap.createClient('https://www.ws1.dnrpa.gov.ar/WS-LEGAUTO/WS-LEGAUTO.wsdl', {
                    wsdl_options: {
                        cert: fs.readFileSync('certs/certificate2.pem'),
                        key: fs.readFileSync('certs/key.key'),
                        passphrase: 'dp19ba84'
                    }
    
                }, function(err, client) {
                    if (err) {
                        console.log('ERROR: Se produjo un error en el servicio SOAP: ', err);
                    }
                    client.setSecurity(new soap.ClientSSLSecurityPFX(
                        fs.readFileSync('certs/30681617783.p12'),
                        'dp19ba84', {
                            strictSSL: false,
                            rejectUnauthorized: false,
                            forever: true,
                        },
                    ));
                    client.consultarDominio({
                        dominio: patente,
                        usuario: 'msm'
                    }, function(err, result, raw, soapHeader) {
                        if (err) {
                            console.error(`ERROR: Fallo al consultar la patente ${patente} - ${new Date().toLocaleString()}'`);
                            return res.send(err)
                        }
                        if (result) {
                            console.log(`INFO: ${new Date().toLocaleString()} Devolviendo resultado desde DNRPA...`);
                            res.json(result);
    
                            // Guardo la patente en memoria
                            setCachePatente(patente, result, ip);
                            return;
                        }
                    });
    
                });
            } else {
                console.log(`INFO: ${new Date().toLocaleString()} La patente ${patente} esta en cache`);
                // Solo muestro propiedad respuesta
                return res.status(200).send(JSON.parse(patenteCacheada).respuesta);
            }
        } catch (e) {
            console.log('ERROR: Se produjo un error en la conexion a DNRPA', e);
        }
    }

    
}


/* Guarda la patente en CACHE DB */
async function setCachePatente(patente, respuestaDNRPA, ip) {

    let datosPatente = {
            dominio: patente,
            respuesta: respuestaDNRPA,
            ip: ip
        }
        // Guardo los datos en String
    return await redisClient.set(patente, JSON.stringify(datosPatente));

};

/*
const setTimeStart = function setTimer() {
    const now = new Date();
    var milsToTime = new Date(now.getFullYear(), now.getMonth(), now.getDate(), 23, 38, 0, 0) - now;

    if (milsToTime < 0) {
        milsToTime += 86400000;
    }

    setTimeout(async function() {
        console.log('INFO: Se cumplio la hora. ', now);
    }, milsToTime);
}
*/

/******* STRESS TEST ************/
//console.log('DEBUG: Ejecutando test...')
//test.testStressRequests();






/********************************************** DEPRECATED CODE*********************************** */ 
/* MIGRACION A MYSQL: Vacia la DB MySQL. Obtiene todas las patentes en memoria y las envia a MySQL. Luego limpia el cache */ 
async function getDataAndSaveToMysql() {

    try {
        const keys1 = await redisClient.keysAsync('*');
        console.log('DEBUG: getDataAndSaveToMysql-> Listado total de patentes obtenidas en el dia: ', keys1);

        // Vaciar datos de MySQL
        await Patente.destroy({
            where: {}
        });

        // Obtener lista de patentes y datos
        const patentsList = await getObjectPatentsList();

        await saveDataToMySQL(patentsList);


        // Vaciar datos en memoria cache luego de haber enviado a mysql
        await redisClient.flushdb(function(err, succeeded) {
            if (err)
                console.error('ERROR: Se produjo un error vaciando la DB en memoria de REDIS: ', err);
            console.log('INFO: Se eliminaron todos los datos en memoria!', succeeded); // will be OK if successfull
        });

        // Quiero ver si tengo datos en cache o no
        const keys = await redisClient.keysAsync('*');
        console.log('INFO: keys luego de enviar a mysql y eliminar', keys);
    } catch (err) {
        throw new Error('ERROR: Se produjo un error en el proceso de migracion a MySQL.', err);
    }
}

/* Devuelve un array de objetos con todas las patentes cacheadas en REDIS */
async function getObjectPatentsList() {
    try {
        // Obtener datos a guardar
        const keys = await redisClient.keysAsync('*');
        const patents = await redisClient.mgetAsync(keys);

        // Convierto a array el (Objeto)
        const arrayDatos = Object.values(patents);
        //arrayDatos.forEach(data => console.log('Imprimiendo elemento;', JSON.parse(data).dominio));

        // Obtengo  las propiedades del array original y las guardo en uno nuevo
        var patentesList = arrayDatos.map(function(params) {
            return {
                "request": JSON.parse(params).dominio,
                "response": JSON.parse(params).respuesta,
                "ip": JSON.parse(params).ip
            };
        });

        console.log('INFO: getObjectPatentsList-> Patentes list', typeof(patentesList), patentesList);
        return patentesList;

    } catch (err) {
        console.error('ERROR: Se produjo un error Obtenido el listado de patentes...', err);
    }
}



/* Genera modelos de datos y los guarda en MySQL*/
async function saveDataToMySQL(listPatentes) {
    try {
        // Transformar datos en modelo. 
        // Creo un model para cada patente recorriendo el array y lo guardo en DB
        listPatentes.forEach(async(patentData) => {
            let request = patentData.request;
            let response = patentData.response;
            let ip = patentData.ip;

            const patente = new Patente({ request, response, ip });

            // Verifico si la patente ya existe
            const existePatente = await Patente.findOne({
                where: {
                    request: request
                }
            });

            if (!existePatente)
            // Enviar modelos a DB MySQL
                await patente.save();
        });
    } catch (err) {
        console.error('ERROR: Se produjo un error guardado los datos en MySQL:', err);
    }

}



