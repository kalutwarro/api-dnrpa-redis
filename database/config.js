//const { Sequelize } = require('sequelize');
const redis = require("redis");
const bluebird = require("bluebird");
const fs = require('fs');
const path = require('path');


/*
const dbConnection = new Sequelize(process.env.NAME_DB, process.env.USER_DB, process.env.PASS_DB, {
    host: process.env.HOST_DB,
    dialect: 'mysql'
    //logging: 'true'
});
*/


// Conversor de callbacks a promesas. Permite el uso de ASYNC en redis
bluebird.promisifyAll(redis);

// Instancia de db redis
const redisClient = redis.createClient();



// Vaciar datos en memoria cache
async function emptyMemory() {
    await redisClient.flushdb(function (err, succeeded) {
        if (err)
            console.error('SUCCESS: Se produjo un error vaciando la DB en memoria de REDIS: ', err);
        console.log('INFO: Se vaciaron todos los datos en memoria!', succeeded); // 'OK' si termina bien
        return;
    });
}


const setTimeStart = function setTimer() {
    const now = new Date();
    var milsToTime = new Date(now.getFullYear(), now.getMonth(), now.getDate(), 10, 25, 0, 0) - now;

    if (milsToTime < 0) {
        milsToTime += 86400000;
    }

    setTimeout(async function() {
        console.log('INFO: Se cumplio la hora. Vaciando memoria...', now.toLocaleString());
        emptyMemory();
    }, milsToTime);
}


// DEBUG
//ejecutarTests();
emptyMemory();
setTimeStart();


/* Obtener keys y values */
async function listarKeys() {
    // Listar todas las keys
    const k = await redisClient.keysAsync('*');
    const values = await redisClient.mgetAsync(k);
    
 
    }


redisClient.on('error', err => {
    console.log('Error ' + err);
});

module.exports = {
    redisClient
    };