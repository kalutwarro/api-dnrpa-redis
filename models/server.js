const express = require('express');
const logger = require('morgan');
const redis = require('redis');

//const cors = require('cors');

//const { dbConnection } = require('../database/config');


class Server {

    constructor() {
        this.app = express();

        this.catchErrors();

        this.port = process.env.PORT;

        // ENDPOINTS URLS
        this.apiPaths = {
            index: '/'
        }

        // Middlewares: funciones que se ejecutan antes de entrar a una ruta
        this.middlewares();

        // Rutas de mi aplicación
        this.routes();

        

        // Conectar a la DB 
        //this.conectarDB();
    }

    catchErrors() {
        process.on('uncaughtException', function(err) {
            if (err.errno === 'EADDRINUSE')
                console.log('ERROR: El puerto solicitado actualmente esta en uso. Indicar otro en el archivo .env', err);
            else {
                console.log('ERROR: Se produjo un error: ', err);
            }
            process.exit(1);
        });

        process.on('uncaughtException', (error, origin) => {
            console.log('----- Uncaught exception -----')
            console.log(error)
            console.log('----- Exception origin -----')
            console.log(origin)
        })
        
        process.on('unhandledRejection', (reason, promise) => {
            console.log('----- Unhandled Rejection at -----')
            console.log(promise)
            console.log('----- Reason -----')
            console.log(reason)
        })
    }

    async conectarDB() {

        try {
            await dbConnection.authenticate();
            console.log('INFO: DB ONLINE')

        } catch (err) {
            throw new Error(err);
        }
    }


    middlewares() {

        // Lectura y parseo del body
        this.app.use(express.json());

        // Directorio Público
        this.app.use(express.static('public'));

        // Logger
        this.app.use(logger('dev'));
    }


    // RUTAS DEL SERVIDOR
    routes() {
        this.app.use(this.apiPaths.index, require('../routes/index'));

    }

    listen() {
        this.app.listen(this.port, () => {
            console.log('SUCCESS: Servidor corriendo en puerto', this.port);
        });
    }



}


module.exports = Server;