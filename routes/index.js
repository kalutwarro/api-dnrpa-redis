process.env["NODE_TLS_REJECT_UNAUTHORIZED"] = 0;

const { Router } = require('express');
const router = Router();
//const { check } = require('express-validator');

const IndexController = require('../controllers/indexController');

router.get('/', function(req, res, next) {
    res.render('start', { title: 'Documentacion' });
});
router.get("/:patente", IndexController.getPatente);

//router.get('/api/save', IndexController.saveMysql);


module.exports = router;